    #
    #
    # ########################################## #
    # #  splatops/cntn-magic-wormhole/README.md #
    # ######################################## #
    #
    # Brought to you by...
    # 
    # ::::::::::::'#######::'########:::'######::
    # :'##::'##::'##.... ##: ##.... ##:'##... ##:
    # :. ##'##::: ##:::: ##: ##:::: ##: ##:::..::
    # '#########: ##:::: ##: ########::. ######::
    # .. ## ##.:: ##:::: ##: ##.....::::..... ##:
    # : ##:. ##:: ##:::: ##: ##::::::::'##::: ##:
    # :..:::..:::. #######:: ##::::::::. ######::
    # ::::::::::::.......:::..::::::::::......:::
    #
    ##################################################
   

# Containerizing magic-wormhole
SplatOps presents a contribution to the magic-wormhole project for building and running in a container.  All examples use and assume the end user has installed Docker and supporting tools.  The easiest way to install Docker outside of native OS packages can be found here: [Get Docker](https://get.docker.com/).

_NOTE: This project is for using magic-wormhole as a **client**._

Link to the original project: [magic-wormhole on Github](https://github.com/warner/magic-wormhole)

## Getting Started
These instructions will walk you through:

* Building the cntn-magic-wormhole Docker image (optional)
* Pulling the cntn-magic-wormhole Docker image (optional)
* Running the cntn-magic-wormhole Docker image
* Configuring cntn-magic-wormhole as an Alias in Your Shell (optional)

## Why
Most people have a basic understanding of what containers are.  The advantage of packaging magic-wormhole into a container include:
* Being able to run the tool natively via an Alpine Linux based system without having to munge with any system level Python packages or deal with outdated local package manager versions.  For example, at the time this was written, the `magic-wormhole` package in Ubuntu 20.04 is version 0.11.2-1 while the latest version from source or pip is 0.12.0.
* Being able to quickly build the container, run the tool, and dispose of everything you've just done without tainting any portion of your system.

### Building the magic-wormhole Docker Image Locally
Clone this repository. Then...
* ```cd cntn-magic-wormhole```
* ```docker build -t splatops/magic-wormhole:latest .```
  
To validate cntn-magic-wormhole is in your local container images...
* ```docker image ls | grep splatops/magic-wormhole```

Notes for local image builds:
* Please review the Dockerfile.  This image is built by leveraging multi-stage to keep it as small as possible.  This still creates an image over 300MB.  If you need additional tools within the container, please modify the Dockerfile to suit your needs.
* Python dependencies are installed using pip in the first phase of the build and then copied into the final container.  If you're trying to leverage pip in an interactive container it isn't there.

### Pulling the magic-wormhole Docker Image from Docker Hub
This is the easiest way to use this project.  If you have no need to modify the Dockerfile and only want to use the container image then...
* ```docker pull splatops/magic-wormhole:latest```
* You can find additional information on [Docker Hub](https://hub.docker.com/r/splatops/magic-wormhole).


### Running the magic-wormhole Docker Image
The magic-wormhole binary is run as a standalone tool against local files which, in most cases, involves sending or receiving a file.  With this in mind we want the container to work against a local copy of the file without having to do any file copying to the running container or deal with creating explicit mount paths.  To solve for this we can leverage `$PWD` in a pattern wherein the container will work on the path the container starts in:
* First, switch to the directory on your machine (this is assuming a Linux/UNIX OS) that is the correct path for the file you want to upload or where you'd like the file to be downloaded:
  * ```cd /home/user/magic-wormhole```
* Finally run the Docker container with the appropriate flags for magic-wormhole.  Below are examples for both sending and receiving files:
  * Send a file:
    * ```docker run -it --tty --rm -v "$PWD:$PWD" -w "$PWD" splatops/magic-wormhole send [file-name-here]```
  * Receive a file:
    * ```docker run -it --tty --rm -v "$PWD:$PWD" -w "$PWD" splatops/magic-wormhole receive [wormhole-code-here]```

### Configuring magic-wormhole as an Alias in Your Shell
Running wormhole in a container provides installation advantages, however issuing a long Docker command each time can be tedius.  Instead, add an alias to your shell!
* ZSH (.zshrc)
  * ```alias wormhole='docker run -it --tty --rm -v `pwd`:`pwd` -w `pwd` splatops/magic-wormhole:latest'```

#### FAQ

* Does this run as root?
  * No.  The container build creates a user with PID 1000 (by default the user is named `wormhole`).
* So I don't need to tell the container to run the `wormhole` binary?
  * No. If you look at the Dockerfile the ENTRYPOINT is the `wormhole` command.  That means everything passed to the container as arguments will be automagically passed to the binary when the container starts.  IF you don't pass anything you get `--help` output.
* How does the container access my local files?
  * We use a bit of a container runtime hack where the current directory is passed to the container and the container mimics that in the container, this happens with the `-v` option flag (bind mount flag).  Then we set the working directory in the container to our working directory in the same manner, this happens with the `-w` flag (working directory flag).
* Where does my new output file get written?
  * The local directory you're in when you start the container. For simplicity sake you'll want to keep the source and destination in the same directory, as directory structure outside of the parent path of your working directory isn't known to the running container.
* I'm trying to build a new version of the container, but I keep getting an old version and the build finishes almost immediately.  What gives?
  * Docker doesn't know when a Git repository changes, so you need to tell it to ignore the build cache and force the build to clone the repo again.  You can do this a variety of ways within the Dockerfile, however the easiest way to work around this is to tell Docker to ignore the build cache during the build command.  Like this:
    * ```docker build --no-cache -t splatops/magic-wormhole:latest .```
* What if I want to do this from inside the container shell but still have access to my local directory I'm currently in on my local machine?
  * This is very similar, but we just need to tell Docker to ignore our ENTRYPOINT from the DOCKERFILE and instead run a shell.  You can do this with the following command which will drop you into a bash shell in the container and with the PATH set correctly for the `cntn-magic-wormhole` binary and you'll have full R/W access to the local machine files you started the container from:
    * ```docker run -it -v "$PWD:$PWD" -w "$PWD" --entrypoint /bin/sh splatops/magic-wormhole:latest```