#
#
# ########################################### #
# #  splatops/cntn-magic-wormhole/Dockerfile #
# ######################################### #
#
# Brought to you by...
# 
# ::::::::::::'#######::'########:::'######::
# :'##::'##::'##.... ##: ##.... ##:'##... ##:
# :. ##'##::: ##:::: ##: ##:::: ##: ##:::..::
# '#########: ##:::: ##: ########::. ######::
# .. ## ##.:: ##:::: ##: ##.....::::..... ##:
# : ##:. ##:: ##:::: ##: ##::::::::'##::: ##:
# :..:::..:::. #######:: ##::::::::. ######::
# ::::::::::::.......:::..::::::::::......:::
#
##################################################
# BUILDER CONTAINER BASE
FROM alpine:3.13 as BUILDER

# BUILDER VARIABLES
ENV \
VERSION=2020.09.22

# BUILDER ENVIRONMENT BUILD
RUN \
echo "### Install dependencies ###" && \
apk update && \
apk add --no-cache build-base libffi-dev openssl-dev python3-dev py3-pip && \
pip3 install magic-wormhole

# MAIN CONTAINER BASE
FROM alpine:3.13

# MAIN ENVIRONMENT BUILD
RUN \
echo "### Install dependencies ###" && \
apk update && \
apk add --no-cache build-base python3

# VARIABLES
ENV \
W_PATH=/opt/magic-wormhole \
W_USERNAME="wormhole"

# CONTAINER STAGING
WORKDIR ${W_PATH}
COPY --from=BUILDER /usr/bin/wormhole /usr/bin/wormhole
COPY --from=BUILDER /usr/lib/python3.8/site-packages /usr/lib/python3.8/site-packages
RUN adduser --uid 1000 --disabled-password --gecos "" "${W_USERNAME}"
RUN chown ${W_USERNAME} ${W_PATH}
USER 1000
ENTRYPOINT [ "wormhole" ]
CMD ["--help"]
